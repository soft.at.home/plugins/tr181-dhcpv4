/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <string.h>

#include <stdarg.h>
#include <setjmp.h>
#include <stddef.h>
#include <cmocka.h>

#include <amxc/amxc_macros.h>

#include <amxut/amxut_timer.h>

#include "../common_dnsmasq/common_dnsmasq.h"
#include "../common_dnsmasq/utils_dnsmasq.h"
#include "../mocks_dnsmasq/mock_amxp_dnsmasq.h"
#include "../mocks_dnsmasq/mock_stdio_dnsmasq.h"
#include "test_module.h"

#include "mod_dhcp_dnsmasq.h"

static void add_ipv4address(amxc_var_t* ipv4, const char* inst_name, const char* address, const char* lease_time) {
    amxc_var_t* inst;
    amxc_var_t* ipv4_data;
    inst = amxc_var_add(amxc_htable_t, ipv4, NULL);
    ipv4_data = amxc_var_add_key(amxc_htable_t, inst, inst_name, NULL);
    amxc_var_add_key(cstring_t, ipv4_data, "IPAddress", address);
    amxc_var_add_key(cstring_t, ipv4_data, "LeaseTimeRemaining", lease_time);
}

static void add_new_option(amxc_var_t* option, const char* option_inst, uint8_t tag, const char* value) {
    amxc_var_t* inst;
    amxc_var_t* option_data;
    inst = amxc_var_add(amxc_htable_t, option, NULL);
    option_data = amxc_var_add_key(amxc_htable_t, inst, option_inst, NULL);
    amxc_var_add_key(uint8_t, option_data, "Tag", tag);
    amxc_var_add_key(cstring_t, option_data, "Value", value);
}

static void create_dummy_backup_data(amxc_var_t* data) {
    amxc_var_t* clients = NULL;
    amxc_var_t* clients_value_1 = NULL;
    amxc_var_t* lease = NULL;
    amxc_var_t* ipv4 = NULL;
    amxc_var_t* option = NULL;

    assert_non_null(data);

    amxc_var_set_type(data, AMXC_VAR_ID_HTABLE);
    clients = amxc_var_add_key(amxc_llist_t, data, "Clients", NULL);

    clients_value_1 = amxc_var_add(amxc_htable_t, clients, NULL);

    lease = amxc_var_add_key(amxc_htable_t, clients_value_1, "client-00:00:00:00:00:01", NULL);
    amxc_var_add_key(cstring_t, lease, "Chaddr", "00:00:00:00:00:01");

    ipv4 = amxc_var_add_key(amxc_llist_t, lease, "IPv4Address", NULL);
    add_ipv4address(ipv4, "1", "192.168.1.3", "2023-11-20T23:48:03.714926513Z");
    add_ipv4address(ipv4, "2", "192.168.1.45", "2023-11-23T23:48:03.714926513Z");

    option = amxc_var_add_key(amxc_llist_t, lease, "Option", NULL);
    add_new_option(option, "1", 50, "c0a8012d");
    add_new_option(option, "2", 61, "01000000000001");
    add_new_option(option, "3", 53, "03");
    add_new_option(option, "4", 54, "c0a80101");
    add_new_option(option, "5", 55, "0102060c0f1a1c79032128292a77f9fc11");
    add_new_option(option, "6", 57, "0240");
    add_new_option(option, "7", 12, "746573745f686f73746e616d65");

}

/**
 * @brief
 * Test done :
 * t-1 :
 *  test : call the start-back-end method
 *  expected : basic configuration is written, the back-end doesn't start as no pool
 * t-2 :
 *  test : call the stop-back-end method
 *  expected : a command to stop dnsmasq is send
 * t-3 :
 *  test : call the enable-dhcp-pool before the start-back-end method
 *  expected : the back-end is started
 */
void test_control_back_end(UNUSED void** state) {
    amxc_var_t config;
    amxc_var_t ret;
    amxc_var_t pool_config;

    amxc_var_init(&config);
    amxc_var_init(&ret);
    amxc_var_init(&pool_config);
    amxc_var_set_type(&config, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&pool_config, AMXC_VAR_ID_HTABLE);

    fill_server_config(&config, true, true, true);
    fill_pool_data(&pool_config, "dummy", 1, "dummy-lan", "192.168.1.2", "192.168.1.250", "255.255.255.0", 360);
    fill_pool_filter(&pool_config, true, "test", "Exact", false, "11:22:33:44:55:66", "ff:ff:ff:00:00:00", false, "All", NULL);

    // t-1
    assert_int_equal(start_dnsmasq(NULL, &config, &ret), 0);
    amxut_timer_go_to_future_ms(10000);
    assert_false(mock_amxp_subproc_started());

    // t-2
    assert_int_equal(stop_dnsmasq(NULL, &config, &ret), 0);
    amxut_timer_go_to_future_ms(10000);
    assert_false(mock_amxp_subproc_started());

    // t-3
    assert_int_equal(enable_dhcp_pool_dnsmasq(NULL, &pool_config, &ret), 0);
    amxut_timer_go_to_future_ms(10000);
    assert_false(mock_amxp_subproc_started());
    assert_int_equal(start_dnsmasq(NULL, &config, &ret), 0);
    amxut_timer_go_to_future_ms(10000);
    assert_true(mock_amxp_subproc_started());

    amxc_var_clean(&config);
    amxc_var_clean(&ret);
    amxc_var_clean(&pool_config);
}

/**
 * @brief
 * Test done :
 * t-1 :
 *  test : change config parameters
 *  expected : dnsmasq should be restarted
 * t-2 :
 *  test : change filter parameters
 *  expected : dnsmasq should be restarted
 * t-3 :
 *  test : don't change config and filter parameters
 *  expected : dnsmasq should be restarted
 * t-4 :
 *  test : add an option to the config
 *  expected : dnsmasq should be restarted
 * t-5 :
 *  test : disable pool
 *  expected : dnsmasq should be restarted
 * t-6 :
 *  test : add a pool with wrong parameter
 *  expected : the call fail but dnsmasq is still up
 */
void test_dhcp_pool(UNUSED void** state) {
    amxc_var_t pool_config;
    amxc_var_t ret;
    amxc_var_t* options = NULL;

    amxc_var_init(&pool_config);
    amxc_var_init(&ret);
    amxc_var_set_type(&pool_config, AMXC_VAR_ID_HTABLE);

    // t-1
    fill_pool_data(&pool_config, "dummy", 1, "dummy-lan", "192.168.1.100", "192.168.1.150", "255.255.255.0", 360);
    fill_pool_filter(&pool_config, true, "test", "Exact", false, "11:22:33:44:55:66", "ff:ff:ff:00:00:00", false, "All", NULL);
    assert_int_equal(enable_dhcp_pool_dnsmasq(NULL, &pool_config, &ret), 0);
    amxut_timer_go_to_future_ms(10000);
    assert_true(mock_amxp_subproc_started());
    assert_true(mock_amxp_subproc_restarted());

    // t-2
    amxc_var_clean(&pool_config);
    amxc_var_init(&pool_config);
    amxc_var_set_type(&pool_config, AMXC_VAR_ID_HTABLE);
    fill_pool_data(&pool_config, "dummy", 1, "dummy-lan", "192.168.1.100", "192.168.1.150", "255.255.255.0", 360);
    fill_pool_filter(&pool_config, true, "test", "Prefix", false, "11:22:33:44:55:66", "ff:ff:ff:ff:00:00", false, "Known", NULL);
    assert_int_equal(enable_dhcp_pool_dnsmasq(NULL, &pool_config, &ret), 0);
    amxut_timer_go_to_future_ms(10000);
    assert_true(mock_amxp_subproc_started());
    assert_true(mock_amxp_subproc_restarted());

    // t-3
    assert_int_equal(enable_dhcp_pool_dnsmasq(NULL, &pool_config, &ret), 0);
    amxut_timer_go_to_future_ms(10000);
    assert_true(mock_amxp_subproc_started());
    assert_false(mock_amxp_subproc_restarted());

    // t-4
    options = amxc_var_add_key(amxc_llist_t, &pool_config, "Option", NULL);
    fill_option(options, "option-12", true, 12, "test");
    assert_int_equal(enable_dhcp_pool_dnsmasq(NULL, &pool_config, &ret), 0);
    amxut_timer_go_to_future_ms(10000);
    assert_true(mock_amxp_subproc_started());
    assert_true(mock_amxp_subproc_restarted());

    // t-5
    assert_int_equal(disable_dhcp_pool_dnsmasq(NULL, &pool_config, &ret), 0);
    amxut_timer_go_to_future_ms(10000);
    assert_true(mock_amxp_subproc_started());
    assert_true(mock_amxp_subproc_restarted());

    // t-6
    amxc_var_clean(&pool_config);
    amxc_var_init(&pool_config);
    amxc_var_set_type(&pool_config, AMXC_VAR_ID_HTABLE);
    fill_pool_data(&pool_config, "dummy", 1, "dummy-lan", "192.168.1.100", "192.168.1.50", "255.255.255.0", 360);
    fill_pool_filter(&pool_config, true, "test", "Prefix", false, "11:22:33:44:55:66", "ff:ff:ff:ff:00:00", false, "Known", NULL);
    assert_int_equal(enable_dhcp_pool_dnsmasq(NULL, &pool_config, &ret), 1);
    amxut_timer_go_to_future_ms(10000);
    assert_true(mock_amxp_subproc_started());

    amxc_var_clean(&pool_config);
    amxc_var_clean(&ret);
}

/**
 * @brief
 * Test done :
 * t-1 :
 *  test : add a static host
 *  expected : dnsmasq should be restarted
 * t-2 :
 *  test : change static host
 *  expected : dnsmasq should be restarted
 * t-3 :
 *  test : add same static host
 *  expected : dnsmasq should not be restarted
 * t-4 :
 *  test : remove static host
 *  expected : dnsmasq should be restarted
 */
void test_static_ip(UNUSED void** state) {
    amxc_var_t data;
    amxc_var_t ret;

    amxc_var_init(&data);
    amxc_var_init(&ret);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    // t-1
    fill_static_ip(&data, 1, "dummy", "master", "dummy-host", "192.168.1.245", "11:22:33:44:55:66");

    assert_int_equal(enable_dhcp_static_ip_dnsmasq(NULL, &data, &ret), 0);
    amxut_timer_go_to_future_ms(10000);
    assert_true(mock_amxp_subproc_started());
    assert_true(mock_amxp_subproc_restarted());

    // t-2
    amxc_var_clean(&data);
    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    fill_static_ip(&data, 1, "dummy", "master", "dummy-host", "192.168.1.100", "11:22:33:44:55:66");

    assert_int_equal(enable_dhcp_static_ip_dnsmasq(NULL, &data, &ret), 0);
    amxut_timer_go_to_future_ms(10000);
    assert_true(mock_amxp_subproc_started());
    assert_true(mock_amxp_subproc_restarted());

    // t-3
    assert_int_equal(enable_dhcp_static_ip_dnsmasq(NULL, &data, &ret), 0);
    amxut_timer_go_to_future_ms(10000);
    assert_true(mock_amxp_subproc_started());
    assert_true(mock_amxp_subproc_restarted());

    // t-4
    assert_int_equal(disable_dhcp_static_ip_dnsmasq(NULL, &data, &ret), 0);
    amxut_timer_go_to_future_ms(10000);
    assert_true(mock_amxp_subproc_started());
    assert_true(mock_amxp_subproc_restarted());

    amxc_var_clean(&data);
    amxc_var_clean(&ret);
}

/**
 * @brief
 * Test done :
 *  test : remove a lease with a list of macs
 *  expected : dnsmasq server is restarted
 */
void test_clean_leases(UNUSED void** state) {
    amxc_var_t mac_list;
    amxc_var_t ret;

    amxc_var_init(&mac_list);
    amxc_var_init(&ret);
    amxc_var_set_type(&mac_list, AMXC_VAR_ID_LIST);

    amxc_var_add(cstring_t, &mac_list, "11:22:33:44:55:66");

    assert_int_equal(clean_leases(NULL, &mac_list, &ret), 0);
    amxut_timer_go_to_future_ms(10000);
    assert_true(mock_amxp_subproc_started());
    assert_true(mock_amxp_subproc_restarted());

    amxc_var_clean(&mac_list);
    amxc_var_clean(&ret);
}

void test_backup_leases(UNUSED void** state) {
    amxc_var_t data;
    const char* expected_leases = "1700783283 00:00:00:00:00:01 192.168.1.45 test_hostname 01:00:00:00:00:00:01";

    amxc_var_init(&data);
    create_dummy_backup_data(&data);
    mock_stdio_register_inputs(true);
    assert_int_equal(update_dhcp_leases_dnsmasq(NULL, &data, NULL), 0);
    mock_stdio_register_inputs(false);

    mock_stdio_check_inputs(expected_leases);
    mock_stdio_free_inputs();

    amxc_var_clean(&data);
}