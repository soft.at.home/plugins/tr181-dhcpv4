# tr181-dhcpv4

# DHCPv4 server plugin
[[_TOC_]]

## Using DHCPv4 Server

To use this plugin, [dnsmasq](https://openwrt.org/docs/guide-user/base-system/dhcp.dnsmasq) need to be installed on the board. As for now the only dhcp sever backend that the plugin support is dnsmasq.

## compile and install

```
make
sudo make install
```

## run test

```
make test
```

## run plugin inside a Ambiorix Docker container

Refer to the Ambiorix tutorials for the steps to launch the Ambiorix Docker container.

### compile/install dependencies: (make + sudo make install)

 * libsahtrace
 * libnetmodel
 * libamxb
 * libamxc
 * libamxd
 * libamxm
 * libamxo
 * libamxp
 * libamxs
 * libamxj
 * libdhcpoptions
### run dependencies

 * amxrt
 * mod-dmext
 * mod-fw-amx
 * dnsmasq :
    - An additional patch to retrieve events can be found [here](https://gitlab.com/soft.at.home/forks/dnsmasq/-/commit/aa260810d5f955938fa85929af7ad887b2da7159)

### activate DHCPv4 server plugin
```
/etc/init/dhcpv4-manager start
```

## Useful information

Details

- DHCP pool and leases are stored in TR-181.
- Changes to the TR-181 data model cause event callbacks to be called which update the UCI or dnsmasq configuration.
- DHCP lease events from dnsmasq cause the TR-181 data model to be updated.

At Startup

- Add all default pool in the config.
- Subscribes to dnsmasq dhcp.ack and dhcp.release events where dhcp_dnsmasq_lease_event is called which adds or removes the lease from TR181 DHCPv4.Server.Pool.instance.Client, and emits the lease:new or lease:remove event.
- Calls ubus dnsmasq leases and adds or updates each DHCP lease in TR181 DHCPv4.Server.Pool.instance.Client.Client-<MAC-Addr>.IPv4Address.
- Calls dm_dhcp_server_maintenance() every 2 minutes to remove inactive or expired DHCP leases from DHCPv4.Server.Pool.instance.Client.

Events called via ODL

- dm:object-added on DHCPv4.Server.Enable call _dhcp_server_enable() which starts or stops the dnsmasq service.
- dm:object-changed on DHCPv4.Server.Enable call _dhcp_server_enable() which starts or stops the dnsmasq service.
- dm:instance-added on DHCPv4.Server.Pool. call _dhcp_server_pool_added() which adds a DHCPv4 server pool in the UCI files.
- dm:instance-changed on DHCPv4.Server.Pool. call _dhcp_server_pool_changed() which updates a DHCPv4 server pool in the UCI files.
- dm:instance-removed on DHCPv4.Server.Pool. call _dhcp_server_pool_removed() which removes a DHCPv4 server pool from the UCI files and closes the DHCP port in the firewall.
- dm:instance-added on DHCPv4.Server.Pool.*.StaticAddress call _dhcp_server_staticip_added() which adds a DHCPv4 server pool static IP address to the UCI files.
- dm:instance-removed on DHCPv4.Server.Pool.*.StaticAddress call _dhcp_server_staticip_removed() which removes a DHCPv4 server pool static IP address from the UCI files.
- dm:object-changed on DHCPv4.Server.Pool.*.StaticAddress.*.[Yiaddr,Chaddr,Enable] call _dhcp_server_staticip_changed() which updates a DHCPv4 server pool static IP address in the UCI files.
- dm:object-changed on DHCPv4.Server.Pool.*.Client.*.IPv4AdressNumberOfEntries call _dhcp_server_client_active() which sets the TR181 DHCPv4.Server.Pool.{i}.Client.{i}.Active flag to false when the client is not using any IP addresses. 

NetModel Query

- A NetModel getAddrs and GetFirstParameter queries are created per IP.Interface to monitor the IPv4 addresses assigned to the network interface.

# Modules option support

| modules | mod-dhcp-dnsmasq | mod-dhcp-uci |
|---|---|---|
| VendorClassID | :white_check_mark: | :white_large_square: |
| Chaddr | :white_check_mark: | :white_large_square: |
| IgnoreIDS | :white_check_mark: | :white_large_square: |
| DDNSUpdate | :white_check_mark: | :white_large_square: |
| EnableFQDN | :white_check_mark: | :white_large_square: |