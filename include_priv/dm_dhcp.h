/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#if !defined(__DM_DHCP_SERVER_H__)
#define __DM_DHCP_SERVER_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <amxc/amxc.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_types.h>
#include <amxd/amxd_transaction.h>
#include <amxo/amxo.h>

typedef int (* dm_dhcp_pool_cb_t) (amxd_object_t* pool);

char* dm_dhcp_get_lease_alias(const char* mac_addr);
bool dm_dhcp_object_is_enabled(amxd_object_t* obj);
bool dm_dhcp_get_dhcp_server_enabled(void);
int dm_dhcp_set_dhcp_server_enabled(bool enable);
char* dm_dhcp_get_interface_path(const char* alias);
int dm_dhcp_get_interface_num_pools(const char* path);
int dm_dhcp_set_pool_static_address_in_trans(amxd_trans_t* trans,
                                             const char* ip_addr,
                                             const char* mac_addr,
                                             bool enable);
int dm_dhcp_add_pool_static_address(const char* alias,
                                    const char* ip_addr,
                                    const char* mac_addr,
                                    bool enable);
int dm_dhcp_set_pool_static_address(const char* alias,
                                    const char* ip_addr,
                                    const char* mac_addr);
int dm_dhcp_delete_pool_static_address(const char* alias, const char* mac_addr);
int dm_dhcp_set_pool_in_trans(amxd_trans_t* trans,
                              const char* alias,
                              const char* interface,
                              bool enable,
                              const char* min_addr,
                              const char* max_addr,
                              const char* subnet_mask,
                              const char* dns_servers,
                              const char* ip_routers,
                              int32_t lease_time);
int dm_dhcp_add_pool(const char* alias,
                     const char* interface,
                     bool enable,
                     const char* min_addr,
                     const char* max_addr,
                     const char* subnet_mask,
                     const char* dns_servers,
                     const char* ip_routers,
                     int32_t lease_time);
int dm_dhcp_delete_pool(const char* alias);
amxd_object_t* dm_dhcp_get_pool_object(const char* alias);
int dm_dhcp_set_pool_min_addr(amxd_object_t* pool, const char* min_addr);
int dm_dhcp_set_pool_interface(amxd_object_t* pool, const char* interface);
int dm_dhcp_set_pool_status(amxd_object_t* pool, const char* status);
char* dm_dhcp_get_pool_status(amxd_object_t* pool);
int dm_dhcp_update_pool_status(amxd_object_t* pool);
int dm_dhcp_for_all_interface_pools(const char* path, dm_dhcp_pool_cb_t fn);
int dm_dhcp_delete_lease(const char* alias, const char* addr);
int dm_dhcp_delete_expired_leases(void);
int dm_dhcp_set_pool_client(const char* alias, const char* mac_addr, const char* ipv4_addr, amxc_ts_t* leasetime, amxc_var_t* options);
int dm_dhcp_set_pool_client_active(const char* path, bool active);
bool dm_dhcp_get_pool_client_active(const char* alias, const char* mac_addr);
int dm_dhcp_delete_inactive_clients(void);
int dm_dhcp_delete_all_pool_clients(void);

void dm_dhcp_set_all_pools_enable(bool enable);
void dm_dhcp_pool_disable_static(amxd_object_t* pool);

int dm_dhcp_set_lease(const char* function_name,
                      amxc_var_t* args,
                      amxc_var_t* ret);

int dm_dhcp_remove_lease(const char* function_name,
                         amxc_var_t* args,
                         amxc_var_t* ret);

int dm_dhcp_static_address_enable(amxd_object_t* pool, amxd_object_t* addr);
int dm_dhcp_static_address_disable(const amxc_var_t* const event_data, amxd_object_t* pool, amxd_object_t* addr);
int dm_dhcp_static_address_assign(amxd_object_t* pool, amxd_object_t* addr);

int _dhcp_main(int reason, amxd_dm_t* dm, amxo_parser_t* parser);

#ifdef __cplusplus
}
#endif

#endif // __DM_DHCP_SERVER_H__
