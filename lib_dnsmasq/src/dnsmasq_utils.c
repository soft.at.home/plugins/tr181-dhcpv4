/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxm/amxm.h>

#include "dnsmasq_utils.h"

/**
 * @brief
 * Transform a list of options returned by dnsmasq to a list of option compatible for the DHCPv4 modules.
 *
 * @param data The variant to add the options to.
 * @param options The option from dnsmasq.
 */
static void dnsmasq_add_options(amxc_var_t* data, amxc_var_t* options) {
    when_null_trace(data, exit, ERROR, "Structure to fill not initialized");
    when_null_trace(options, exit, ERROR, "No option to add");

    amxc_var_for_each(option, options) {
        uint32_t tag = 0;
        char stag[256];
        const char* value = NULL;

        tag = GET_UINT32(option, "tag");
        snprintf(stag, 255, "%d", tag);
        value = GET_CHAR(option, "raw");

        amxc_var_add_key(cstring_t, data, stag, value);
    }
exit:
    return;
}

/**
 * @brief
 * Construct a variant with all the needed info for the DHCPv4 modules than call the add call back function.
 * The dnsmasq library need to be initialised to call this function.
 *
 * @param lease the lease from dnsmasq to send.
 * @param network the interface that the lease is comming from.
 * @param mac_addr the mac address of the client with that lease.
 * @param ipv4 the IPv4 address given in that lease.
 */
void dnsmasq_send_lease(const amxc_var_t* lease,
                        const char* network,
                        const char* mac_addr,
                        const char* ipv4) {
    const char* time = NULL;
    amxc_var_t* options = NULL;
    amxc_var_t* lease_options = NULL;
    amxc_var_t data;
    amxc_ts_t expires;

    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    when_false_trace(dnsmasq.add_ok, exit, ERROR, "No add function set");
    when_null_trace(lease, exit, ERROR, "No lease given to send");
    when_null_trace(network, exit, ERROR, "Lease is not from a pool");
    when_null_trace(mac_addr, exit, ERROR, "Lease is not for a device");
    when_null_trace(ipv4, exit, ERROR, "Lease does not have an ipv4");

    amxc_var_add_key(cstring_t, &data, "Alias", network);
    amxc_var_add_key(cstring_t, &data, "Chaddr", mac_addr);
    amxc_var_add_key(cstring_t, &data, "IPv4Address", ipv4);

    time = GET_CHAR(lease, "expires");
    if(time != NULL) {
        amxc_ts_parse(&expires, time, strlen(time));
        amxc_var_add_key(amxc_ts_t, &data, "LeaseTimeRemaining", &expires);
    }

    options = GET_ARG(lease, "options_request");
    lease_options = amxc_var_add_key(amxc_htable_t, &data, "Option", NULL);

    dnsmasq_add_options(lease_options, options);

    dnsmasq.add_lease(&data);
exit:
    amxc_var_clean(&data);
    return;
}

/**
 * @brief
 * Construct a variant with all the needed info for the DHCPv4 modules than call the remove call back function.
 * The dnsmasq library need to be initialised to call this function.
 *
 * @param lease the lease from dnsmasq to send.
 * @param network the interface that the lease is comming from.
 * @param mac_addr the mac address of the client with that lease.
 * @param ipv4 the IPv4 address given in that lease.
 */
static void dnsmasq_delete_lease(const amxc_var_t* lease,
                                 const char* network,
                                 const char* mac_addr,
                                 const char* ipv4) {
    amxc_var_t data;

    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    when_false_trace(dnsmasq.del_ok, exit, ERROR, "No delete function set");
    when_null_trace(lease, exit, ERROR, "No lease given to send");
    when_null_trace(mac_addr, exit, ERROR, "Lease is not for a device");
    when_null_trace(ipv4, exit, ERROR, "Lease does not have an ipv4");

    amxc_var_add_key(cstring_t, &data, "Alias", network);
    amxc_var_add_key(cstring_t, &data, "Chaddr", mac_addr);
    amxc_var_add_key(cstring_t, &data, "IPv4Address", ipv4);

    dnsmasq.delete_lease(&data);
exit:
    amxc_var_clean(&data);
    return;
}

/**
 * @brief
 * Adds or removes a TR181 DHCPv4.Server.Pool.{i}.Client instance to the data model and sends a lease event.
 * Called when the dhcp.ack or dhcp.release events are generated by the dnsmasq service.
 *
 * @param sig_name event name
 * @param data event data
 * @param priv private data associated with event subscription
 */
void dhcp_dnsmasq_lease_event(UNUSED const char* const sig_name,
                              const amxc_var_t* const data,
                              UNUSED void* const priv) {
    const char* event = NULL;
    const char* mac = NULL;
    const char* ip = NULL;
    const char* network = NULL;
    char* curs = NULL;

    when_null_trace(data, exit, ERROR, "Invalid data argument");
    event = GET_CHAR(data, "notification");
    when_null_trace(event, exit, ERROR, "Lease event is missing notification");
    mac = GET_CHAR(data, "mac");
    when_null_trace(mac, exit, ERROR, "Lease event is missing mac");
    ip = GET_CHAR(data, "ip");
    when_null_trace(ip, exit, ERROR, "Lease event is missing ip");
    network = GET_CHAR(data, "net");
    when_null_trace(network, exit, ERROR, "Lease event is missing network");

    when_false_trace(strcmp(ip, "0.0.0.0") != 0, exit, INFO, "Current version of dnsmasq patch sends a spurious event when negotiating a different IP");

    // remove tag at the end
    curs = strrchr(network, '_');
    when_null_trace(curs, exit, ERROR, "No tag at the end of the network");
    *curs = '\0';

    if(strncmp(event, DHCPACK, strlen(DHCPACK)) == 0) {
        SAH_TRACEZ_INFO(ME, "EVENT - Adding new DHCPv4 lease '%s' '%s' '%s'", ip, mac, network);
        dnsmasq_send_lease(data, network, mac, ip);
    } else if(strncmp(event, DHCPRELEASE, strlen(DHCPRELEASE)) == 0) {
        SAH_TRACEZ_INFO(ME, "EVENT - Removing DHCPv4 lease '%s' '%s' '%s'", ip, mac, network ? network : "NULL");
        dnsmasq_delete_lease(data, network, mac, ip);
    } else {
        SAH_TRACEZ_ERROR(ME, "Unknown dnsmasq lease event '%s' '%s' '%s' '%s'", event, ip, mac, network ? network : "NULL");
    }
exit:
    return;
}