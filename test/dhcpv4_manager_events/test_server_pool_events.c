/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_transaction.h>

#include <amxut/amxut_bus.h>
#include <amxut/amxut_dm.h>

#include "../common/common.h"
#include "../common/dm_utils.h"
#include "../mocks/mock_amxm.h"
#include "../mocks/mock_netmodel.h"
#include "test_events.h"

#define SERVER_PATH "DHCPv4Server."

/**
 * Not tested :
 *  - test of defaults pools loading
 *  - test of wrong range, range verification occurs on back-end
 *  - test of UserID parameters
 *  - test of ClientID parameters
 */

/**
 * @brief
 * Tests done :
 * test : add a new pool
 * expected :
 *      the new pool with new interface is added, netmodel callbacks are created,
 *      signal are send to the back-end to add the new range and the firewall is opened.
 */
void test_pool(UNUSED void** state) {

    amxut_dm_load_odl("../common/odl/dummy_pools_non_defaults.odl");
    amxut_bus_handle_events();

    amxut_dm_param_equals(bool, SERVER_PATH "Pool.dummy-1.", "Enable", true);
    amxut_dm_param_equals(bool, SERVER_PATH "Pool.dummy-2.", "Enable", true);
    amxut_dm_param_equals(bool, SERVER_PATH "Pool.dummy-3.", "Enable", true);
    amxut_dm_param_equals(bool, SERVER_PATH "Pool.dummy-4.", "Enable", true);

    expect_amxm_execute("fw", "set_service", NULL, 1);
    expect_amxm_execute("mod-dhcp-dnsmasq", "enable-dhcp-pool", NULL, 2);
    mock_netmodel_trigger_call_backs();
    amxut_bus_handle_events();

}

/**
 * @brief
 * Tests done :
 * t-1 :
 *  test : change pool order with an existing one (higher in precedence)
 *  expected :
 *      pool order is changed, all pool lower in precedence are increased by 1,
 *      information es passed to the back-end.
 * t-2 :
 *  test : change pool order with an existing one (lower in precedence)
 *  expected :
 *      pool order is changed, all pool higher in precedence are decreased by 1,
 *      information is passed to the back-end
 */
void test_pool_order(UNUSED void** state) {
    amxut_bus_handle_events();

    amxut_dm_param_equals(uint32_t, SERVER_PATH "Pool.dummy-1.", "Order", 1);
    amxut_dm_param_equals(uint32_t, SERVER_PATH "Pool.dummy-2.", "Order", 2);
    amxut_dm_param_equals(uint32_t, SERVER_PATH "Pool.dummy-3.", "Order", 3);
    amxut_dm_param_equals(uint32_t, SERVER_PATH "Pool.dummy-4.", "Order", 4);

    // t-1
    assert_dm_set_param(uint32_t, SERVER_PATH "Pool.dummy-4.", "Order", 1);
    amxut_bus_handle_events();

    expect_amxm_execute("mod-dhcp-dnsmasq", "enable-dhcp-pool", NULL, 4);
    mock_netmodel_trigger_call_backs();
    amxut_bus_handle_events();

    amxut_dm_param_equals(uint32_t, SERVER_PATH "Pool.dummy-1.", "Order", 2);
    amxut_dm_param_equals(uint32_t, SERVER_PATH "Pool.dummy-2.", "Order", 3);
    amxut_dm_param_equals(uint32_t, SERVER_PATH "Pool.dummy-3.", "Order", 4);
    amxut_dm_param_equals(uint32_t, SERVER_PATH "Pool.dummy-4.", "Order", 1);

    // t-2
    assert_dm_set_param(uint32_t, SERVER_PATH "Pool.dummy-4.", "Order", 4);
    amxut_bus_handle_events();

    expect_amxm_execute("mod-dhcp-dnsmasq", "enable-dhcp-pool", NULL, 4);
    mock_netmodel_trigger_call_backs();
    amxut_bus_handle_events();

    amxut_dm_param_equals(uint32_t, SERVER_PATH "Pool.dummy-1.", "Order", 1);
    amxut_dm_param_equals(uint32_t, SERVER_PATH "Pool.dummy-2.", "Order", 2);
    amxut_dm_param_equals(uint32_t, SERVER_PATH "Pool.dummy-3.", "Order", 3);
    amxut_dm_param_equals(uint32_t, SERVER_PATH "Pool.dummy-4.", "Order", 4);
}

/**
 * @brief
 * Tests done :
 * t-1 :
 *  test : change lease time of a pool
 *  expected :
 *      firewall is closed and pool is disabled, then firewall is open and pool is enabled.
 * t-2 :
 *  test : change some options of the pool
 *  expected : the information is passed to the back-end
 * t-3 :
 *  test : change the domain name of the pool
 *  expected : the information is passed to the back-end
 */
void test_pool_config_changed(UNUSED void** state) {
    amxc_var_t pool_param;
    amxut_bus_handle_events();

    amxc_var_init(&pool_param);
    amxc_var_set_type(&pool_param, AMXC_VAR_ID_HTABLE);

    // t-1
    amxut_dm_param_equals(int32_t, SERVER_PATH "Pool.dummy-1.", "LeaseTime", 43200);

    assert_dm_set_param(uint32_t, SERVER_PATH "Pool.dummy-1.", "LeaseTime", 100);
    amxc_var_add_key(uint32_t, &pool_param, "LeaseTime", 100);
    amxut_bus_handle_events();

    expect_amxm_execute("mod-dhcp-dnsmasq", "enable-dhcp-pool", &pool_param, 1);
    mock_netmodel_trigger_call_backs();
    amxut_bus_handle_events();

    // t-2
    amxut_dm_param_equals(cstring_t, SERVER_PATH "Pool.dummy-1.", "MaxAddress", "192.168.1.254");
    amxut_dm_param_equals(cstring_t, SERVER_PATH "Pool.dummy-1.", "MinAddress", "192.168.1.2");

    assert_dm_set_param(cstring_t, SERVER_PATH "Pool.dummy-1.", "MaxAddress", "192.168.1.100");
    amxc_var_add_key(cstring_t, &pool_param, "MaxAddress", "192.168.1.100");
    amxut_bus_handle_events();

    expect_amxm_execute("mod-dhcp-dnsmasq", "enable-dhcp-pool", &pool_param, 1);
    mock_netmodel_trigger_call_backs();
    amxut_bus_handle_events();

    // t-3
    amxut_dm_param_equals(cstring_t, SERVER_PATH "Pool.dummy-1.", "DomainName", "");

    assert_dm_set_param(cstring_t, SERVER_PATH "Pool.dummy-1.", "DomainName", "home.arpa");
    amxc_var_add_key(cstring_t, &pool_param, "DomainName", "home.arpa");
    amxut_bus_handle_events();

    expect_amxm_execute("mod-dhcp-dnsmasq", "enable-dhcp-pool", &pool_param, 1);
    mock_netmodel_trigger_call_backs();
    amxut_bus_handle_events();

    amxc_var_clean(&pool_param);
}

/**
 * @brief
 * Tests done :
 * t-1 :
 *  test : change of VendorClassID parameters
 *  expected : trigger an update of the back-end
 * t-2 :
 *  test : change of Chaddr parameters
 *  expected : trigger an update of the back-end
 * t-3 :
 *  test : change of AllowedDevices parameter
 *  expected : trigger an update of the back-end
 * t-4 :
 *  test : change of Tag parameter
 *  expected : trigger an update of the back-end
 */
void test_pool_filter(UNUSED void** state) {
    amxc_var_t pool_param;
    amxut_bus_handle_events();

    amxc_var_init(&pool_param);
    amxc_var_set_type(&pool_param, AMXC_VAR_ID_HTABLE);

    // t-1
    amxut_dm_param_equals(cstring_t, SERVER_PATH "Pool.dummy-1.", "VendorClassID", "");
    amxut_dm_param_equals(bool, SERVER_PATH "Pool.dummy-1.", "VendorClassIDExclude", false);
    amxut_dm_param_equals(cstring_t, SERVER_PATH "Pool.dummy-1.", "VendorClassIDMode", "Exact");

    assert_dm_set_param(cstring_t, SERVER_PATH "Pool.dummy-1.", "VendorClassID", "test-dummy");
    assert_dm_set_param(bool, SERVER_PATH "Pool.dummy-1.", "VendorClassIDExclude", true);
    assert_dm_set_param(cstring_t, SERVER_PATH "Pool.dummy-1.", "VendorClassIDMode", "Prefix");

    amxc_var_add_key(cstring_t, &pool_param, "VendorClassID", "test-dummy");
    amxc_var_add_key(bool, &pool_param, "VendorClassIDExclude", true);
    amxc_var_add_key(cstring_t, &pool_param, "VendorClassIDMode", "Prefix");

    expect_amxm_execute("mod-dhcp-dnsmasq", "enable-dhcp-pool", &pool_param, 3);
    amxut_bus_handle_events();

    // t-2
    amxut_dm_param_equals(cstring_t, SERVER_PATH "Pool.dummy-1.", "Chaddr", "");
    amxut_dm_param_equals(cstring_t, SERVER_PATH "Pool.dummy-1.", "ChaddrMask", "");
    amxut_dm_param_equals(bool, SERVER_PATH "Pool.dummy-1.", "ChaddrExclude", false);

    assert_dm_set_param(cstring_t, SERVER_PATH "Pool.dummy-1.", "Chaddr", "11:22:33:44:55:66");
    assert_dm_set_param(cstring_t, SERVER_PATH "Pool.dummy-1.", "ChaddrMask", "ff:ff:ff:00:00:00");
    assert_dm_set_param(bool, SERVER_PATH "Pool.dummy-1.", "ChaddrExclude", true);

    amxc_var_add_key(cstring_t, &pool_param, "Chaddr", "11:22:33:44:55:66");
    amxc_var_add_key(cstring_t, &pool_param, "ChaddrMask", "ff:ff:ff:00:00:00");
    amxc_var_add_key(bool, &pool_param, "ChaddrExclude", true);

    expect_amxm_execute("mod-dhcp-dnsmasq", "enable-dhcp-pool", &pool_param, 3);
    amxut_bus_handle_events();

    // t-3
    amxut_dm_param_equals(cstring_t, SERVER_PATH "Pool.dummy-1.", "AllowedDevices", "All");

    assert_dm_set_param(cstring_t, SERVER_PATH "Pool.dummy-1.", "AllowedDevices", "Known");

    amxc_var_add_key(cstring_t, &pool_param, "AllowedDevices", "Known");

    expect_amxm_execute("mod-dhcp-dnsmasq", "enable-dhcp-pool", &pool_param, 1);
    amxut_bus_handle_events();

    // t-4
    amxut_dm_param_equals(cstring_t, SERVER_PATH "Pool.dummy-1.", "Tag", "master");

    assert_dm_set_param(cstring_t, SERVER_PATH "Pool.dummy-1.", "Tag", "extender");

    amxc_var_add_key(cstring_t, &pool_param, "Tag", "extender");

    expect_amxm_execute("mod-dhcp-dnsmasq", "enable-dhcp-pool", &pool_param, 1);
    amxut_bus_handle_events();

    amxc_var_clean(&pool_param);
}

/**
 * @brief
 * Tests done :
 * test : remove pool
 * expected :
 *      The new pool is removed, the back-end remove the pool and the firewall is closed.
 */
void test_pool_remove(UNUSED void** state) {
    expect_amxm_execute("mod-dhcp-dnsmasq", "disable-dhcp-pool", NULL, 1);
    expect_amxm_execute("fw", "delete_service", NULL, 1);
    expect_amxm_execute("mod-dhcp-dnsmasq", "disable-dhcp-pool", NULL, 1);
    assert_dm_rm_inst(SERVER_PATH "Pool.", "dummy-3", 0);
    assert_dm_rm_inst(SERVER_PATH "Pool.", "dummy-4", 0);
    amxut_bus_handle_events();
}

/**
 * @brief
 * Tests done :
 * t-1 :
 *  test : add a pool option
 *  expected : the option is forwarded to the back-end
 * t-2 :
 *  test : update value of an option
 *  expected : the change is forwarded to the back-end
 * t-3 :
 *  test : remove the option
 *  expected : the back-end is updated
 */
void test_pool_option(UNUSED void** state) {
    // t-1
    amxut_dm_param_equals(bool, SERVER_PATH "Pool.dummy-1.", "Enable", true);

    add_pool_option(SERVER_PATH "Pool.dummy-1.Option.", 14, "74657374", true);
    expect_amxm_execute("mod-dhcp-dnsmasq", "enable-dhcp-pool", NULL, 1);
    amxut_bus_handle_events();

    // t-2
    amxut_dm_param_set(cstring_t, SERVER_PATH "Pool.dummy-1.Option.1.", "Value", "746573742d32");
    expect_amxm_execute("mod-dhcp-dnsmasq", "enable-dhcp-pool", NULL, 1);
    amxut_bus_handle_events();

    // t-3
    assert_dm_rm_inst(SERVER_PATH "Pool.dummy-1.Option.", NULL, 1);
    expect_amxm_execute("mod-dhcp-dnsmasq", "enable-dhcp-pool", NULL, 1);
    amxut_bus_handle_events();
}

/**
 * @brief
 * Tests done :
 * t-1 :
 *  test : add a new WINSServer value
 *  expected : New option [Tag == 44] created
 * t-2 :
 *  test : update the value of the WINSServer
 *  expected : The option [Tag == 44] will be updated
 * t-3 :
 *  test : set empty string value to WINSServer
 *  expected : The option [Tag == 44] will be deleted
 */
void test_pool_winsserver_option(UNUSED void** state) {
    const char* hex_value = NULL;
    const char* winsserver = NULL;

    // t-1
    winsserver = "192.168.1.1";
    hex_value = "C0A80101";

    assert_dm_set_param(cstring_t, SERVER_PATH "Pool.dummy-1.", "WINSServer", winsserver);
    expect_amxm_execute("mod-dhcp-dnsmasq", "enable-dhcp-pool", NULL, 1);
    amxut_bus_handle_events();
    amxut_dm_param_equals(cstring_t, SERVER_PATH "Pool.dummy-1.Option.cpe-dhcp-44",
                          "Value", hex_value);

    // t-2
    winsserver = "192.168.1.1, 192.168.1.2";
    hex_value = "C0A80101C0A80102";

    assert_dm_set_param(cstring_t, SERVER_PATH "Pool.dummy-1.", "WINSServer", winsserver);
    expect_amxm_execute("mod-dhcp-dnsmasq", "enable-dhcp-pool", NULL, 1);
    amxut_bus_handle_events();
    amxut_dm_param_equals(cstring_t, SERVER_PATH "Pool.dummy-1.Option.cpe-dhcp-44",
                          "Value", hex_value);

    // t-3
    winsserver = "";
    hex_value = NULL;

    assert_dm_set_param(cstring_t, SERVER_PATH "Pool.dummy-1.", "WINSServer", winsserver);
    expect_amxm_execute("mod-dhcp-dnsmasq", "enable-dhcp-pool", NULL, 1);
    amxut_bus_handle_events();
    assert_dm_check_inst(SERVER_PATH "Pool.dummy-1.Option.cpe-dhcp-44", false);
}