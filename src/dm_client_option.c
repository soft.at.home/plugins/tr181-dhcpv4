/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdlib.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "dhcp_priv.h"
#include "dm_dhcp.h"
#include "dhcp_event.h"
#include "dhcp_action.h"

#include <amxb/amxb_operators.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <v4v6option.h>

#define ME "dhcpv4s"

typedef enum {
    DNS_HOST_ARG_HOSTNAME  = 1 << 0,
    DNS_HOST_ARG_POOL      = 1 << 1,
    DNS_HOST_ARG_CLIENT    = 1 << 2,
} dns_set_host_args_t;
#define DNS_HOST_ARG_ALL (~(dns_set_host_args_t) 0)

static void add_pool_arguments(amxc_var_t* args, amxd_object_t* option_12) {
    amxd_object_t* pool_obj = amxd_object_findf(option_12, ".^.^.^.^");
    const char* interface = NULL;

    interface = GET_CHAR(amxd_object_get_param_value(pool_obj, "Interface"), NULL);
    when_str_empty_trace(interface, exit, ERROR, "Pool's Interface parameter is empty");

    amxc_var_add_key(cstring_t, args, "Interface", interface);
exit:
    return;
}

static void add_client_arguments(amxc_var_t* args, amxd_object_t* option_12) {
    amxd_object_t* client_obj = amxd_object_findf(option_12, ".^.^");
    amxc_var_t* ip_addresses = NULL;
    uint32_t count = 0;
    bool active = amxd_object_get_value(bool, client_obj, "Active", NULL);

    ip_addresses = amxc_var_add_key(amxc_llist_t, args, "IPAddresses", NULL);

    amxd_object_for_each(instance, it, amxd_object_findf(client_obj, ".IPv4Address.")) {
        amxd_object_t* ip_address_obj = amxc_container_of(it, amxd_object_t, it);
        const char* ip_address = GET_CHAR(amxd_object_get_param_value(ip_address_obj, "IPAddress"), NULL);
        amxc_var_add(cstring_t, ip_addresses, ip_address);
        count++;
    }

    amxc_var_cast(ip_addresses, AMXC_VAR_ID_CSV_STRING);

    amxc_var_add_key(bool, args, "Enable", active && (count > 0));
}

static bool add_hostname_arguments(amxc_var_t* args, amxd_object_t* option_12) {
    bool rv = false;
    uint32_t length = 0;
    unsigned char* value_bin = NULL;
    amxc_var_t* host_name = NULL;
    const char* value_hex = GET_CHAR(amxd_object_get_param_value(option_12, "Value"), NULL);

    when_str_empty(value_hex, exit);

    value_bin = dhcpoption_option_convert2bin(value_hex, &length);
    when_null_trace(value_bin, exit, ERROR, "Failed to convert DHCP option(12) HostName to binary: '%s'", value_hex);

    host_name = amxc_var_add_new_key(args, "Name");
    dhcpoption_v4parse(host_name, 12, length, value_bin);

    when_false_trace(amxc_var_type_of(host_name) == AMXC_VAR_ID_CSTRING, exit, ERROR, "Failed to convert DHCP option(12) HostName: '%s'", value_hex);

    rv = true;
exit:
    free(value_bin);
    return rv;
}

static bool args_key_match(amxc_var_t* a, amxc_var_t* b, const char* key) {
    bool rv = false;
    int cmp = -1;
    amxc_var_t* a_var = GET_ARG(a, key);
    amxc_var_t* b_var = GET_ARG(b, key);
    when_failed(amxc_var_compare(a_var, b_var, &cmp), exit);
    when_failed(cmp, exit);
    rv = true;
exit:
    return rv;
}

static void args_remove_synced_keys(amxc_var_t* synced_args, amxc_var_t* syncing_args, const char* key) {
    amxc_var_t* var = NULL;
    when_false(args_key_match(synced_args, syncing_args, key), exit);
    var = amxc_var_take_key(syncing_args, key);
    amxc_var_delete(&var);
exit:
    return;
}

static void mark_args_as_synced(amxc_var_t* synced_args, amxc_var_t* syncing_args) {
    amxc_var_for_each(syncing_var, syncing_args) {
        const char* key = amxc_var_key(syncing_var);
        amxc_var_t* synced_var = GET_ARG(synced_args, key);
        if(synced_var == NULL) {
            synced_var = amxc_var_add_new_key(synced_args, key);
        }
        amxc_var_move(synced_var, syncing_var);
    }
}

static bool set_host(amxd_object_t* option_12, dns_set_host_args_t unsynced_args) {
    amxb_bus_ctx_t* bus = NULL;
    amxc_var_t* synced_args = NULL;
    const char* alias = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    int amxb_rv = -1;
    bool rv = false;

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_init(&ret);

    when_null_trace(option_12, exit, ERROR, "No option 12 provided");
    when_str_empty(GET_CHAR(amxd_object_get_param_value(option_12, "Value"), NULL), exit);

    bus = amxb_be_who_has("DNS.");
    when_null_trace(bus, exit, ERROR, "No bus found for 'DNS.'");

    if(option_12->priv == NULL) {
        amxc_var_new((amxc_var_t**) &option_12->priv);
        amxc_var_set_type((amxc_var_t*) option_12->priv, AMXC_VAR_ID_HTABLE);
    }
    synced_args = (amxc_var_t*) option_12->priv;
    alias = GET_CHAR(synced_args, "Alias");

    if(alias == NULL) {
        unsynced_args = DNS_HOST_ARG_ALL;
    }

    if((unsynced_args & DNS_HOST_ARG_HOSTNAME)) {
        when_false_trace(add_hostname_arguments(&args, option_12), exit, ERROR, "Failed to parse hostname for DNS host");
        args_remove_synced_keys(synced_args, &args, "Name");
    }

    if(unsynced_args & DNS_HOST_ARG_POOL) {
        add_pool_arguments(&args, option_12);
        args_remove_synced_keys(synced_args, &args, "Interface");
    }

    if(unsynced_args & DNS_HOST_ARG_CLIENT) {
        add_client_arguments(&args, option_12);
        args_remove_synced_keys(synced_args, &args, "Enable");
        args_remove_synced_keys(synced_args, &args, "IPAddresses");
    }

    if(GET_CHAR(synced_args, "Origin") == NULL) {
        amxc_var_add_key(cstring_t, &args, "Origin", "DHCPv4");
    }
    if(GET_ARG(synced_args, "Exclusive") == NULL) {
        amxc_var_add_key(bool, &args, "Exclusive", true);
    }

    when_null_trace(amxc_var_get_first(&args), exit, INFO, "Nothing to sync to dns host");

    if(alias != NULL) {
        amxc_var_add_key(cstring_t, &args, "Alias", alias);
    }

    amxb_rv = amxb_call(bus, "DNS.", "SetHost", &args, &ret, 5);

    when_false_trace(amxb_rv == AMXB_STATUS_OK, exit, ERROR, "Failed to exec DNS.SetHost: %d", amxb_rv);

    if(alias == NULL) {
        amxc_var_t* alias_var = amxc_var_add_new_key(synced_args, "Alias");
        amxc_var_push(cstring_t, alias_var, amxc_var_take(cstring_t, GETI_ARG(&ret, 0)));
    }
    mark_args_as_synced(synced_args, &args);

    rv = true;
exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return rv;
}

static bool remove_host(amxd_object_t* option_12) {
    amxc_var_t* synced_args = NULL;
    amxb_bus_ctx_t* bus = NULL;
    const char* alias = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    int amxb_rv = -1;
    bool rv = false;

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_init(&ret);

    when_null_trace(option_12, exit, ERROR, "No option 12 provided");
    synced_args = (amxc_var_t*) option_12->priv;
    when_null(synced_args, exit);

    bus = amxb_be_who_has("DNS.");
    when_null_trace(bus, exit, ERROR, "No bus found for 'DNS.'");

    alias = GET_CHAR(synced_args, "Alias");
    when_str_empty_trace(alias, exit, WARNING, "No alias known to remove dns host");

    amxc_var_add_key(cstring_t, &args, "Alias", alias);

    amxb_rv = amxb_call(bus, "DNS.", "RemoveHost", &args, &ret, 5);
    when_false_trace(amxb_rv == AMXB_STATUS_OK, exit, ERROR, "Failed to exec DNS.RemoveHost: %d", amxb_rv);

    rv = true;
exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    if(synced_args != NULL) {
        amxc_var_delete(&synced_args);
        option_12->priv = NULL;
    }
    return rv;
}

static bool remove_host_ip(amxd_object_t* option_12, const char* ip_addr) {
    amxc_var_t* synced_args = NULL;
    amxb_bus_ctx_t* bus = NULL;
    const char* alias = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    int amxb_rv = -1;
    bool rv = false;

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_init(&ret);

    when_null_trace(option_12, exit, ERROR, "No option 12 provided");
    when_str_empty_status(ip_addr, exit, rv = true);
    synced_args = (amxc_var_t*) option_12->priv;
    when_null(synced_args, exit);

    bus = amxb_be_who_has("DNS.");
    when_null_trace(bus, exit, ERROR, "No bus found for 'DNS.'");

    alias = GET_CHAR(synced_args, "Alias");
    when_str_empty_trace(alias, exit, WARNING, "No alias known to remove dns host");

    amxc_var_add_key(cstring_t, &args, "Alias", alias);
    amxc_var_add_key(csv_string_t, &args, "IPAddresses", ip_addr);

    amxb_rv = amxb_call(bus, "DNS.", "RemoveIPFromHost", &args, &ret, 5);
    when_false_trace(amxb_rv == AMXB_STATUS_OK, exit, ERROR, "Failed to exec DNS.RemoveHost: %d", amxb_rv);

    rv = true;
exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return rv;
}

void _dhcps_client_option_hostname_added(UNUSED const char* const event_name,
                                         const amxc_var_t* const data,
                                         UNUSED void* const priv) {
    amxd_object_t* templ = amxd_dm_signal_get_object(dhcp_get_dm(), data);
    amxd_object_t* option_12 = amxd_object_get_instance(templ, NULL, GET_UINT32(data, "index"));
    amxc_var_t* params = GET_ARG(data, "parameters");
    const char* value_hex = GET_CHAR(params, "Value");
    uint32_t tag = GET_UINT32(params, "Tag");

    when_false_trace(tag == 12, exit, WARNING, "Unexpected DHCP option %u, HostName option is 12 (rfc2132)", tag);

    when_str_empty(value_hex, exit);

    set_host(option_12, DNS_HOST_ARG_ALL);
exit:
    return;
}

static void dhcps_client_option_12_changed(amxd_object_t* option_12) {
    const char* value_hex = GET_CHAR(amxd_object_get_param_value(option_12, "Value"), NULL);
    if(!str_empty(value_hex)) {
        set_host(option_12, DNS_HOST_ARG_HOSTNAME);
    } else {
        remove_host(option_12);
    }
}

void _dhcps_client_option_changed(UNUSED const char* const event_name,
                                  const amxc_var_t* const data,
                                  UNUSED void* const priv) {
    uint32_t tag = 0;
    amxd_object_t* option = amxd_dm_signal_get_object(dhcp_get_dm(), data);

    when_null_trace(option, exit, ERROR, "Couldn't resolve signal object: %s", GET_CHAR(data, "object"));

    tag = amxd_object_get_uint32_t(option, "Tag", NULL);

    switch(tag) {
    case 12:
        dhcps_client_option_12_changed(option);
        break;
    }
exit:
    return;
}

amxd_status_t _dhcps_client_option_destroy(amxd_object_t* option,
                                           UNUSED amxd_param_t* param,
                                           amxd_action_t reason,
                                           UNUSED const amxc_var_t* const args,
                                           UNUSED amxc_var_t* const retval,
                                           UNUSED void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    uint32_t tag = amxd_object_get_value(uint32_t, option, "Tag", NULL);

    when_null(option, exit);
    when_true_status(reason != action_object_destroy,
                     exit,
                     status = amxd_status_function_not_implemented);

    when_true_status(amxd_object_get_type(option) == amxd_object_template,
                     exit,
                     status = amxd_status_ok);

    switch(tag) {
    case 12:
        remove_host(option);
        break;
    }

    status = amxd_status_ok;
exit:
    return status;
}

static void dhcps_client_option_ipaddress_update_dns_host(amxd_object_t* ipaddress) {
    amxd_object_t* option_12 = amxd_object_findf(ipaddress, ".^.^.Option.[Tag == 12].");
    when_null(option_12, exit);
    set_host(option_12, DNS_HOST_ARG_CLIENT | DNS_HOST_ARG_POOL);
exit:
    return;
}

void _dhcps_client_option_hostname_ipaddress_added(UNUSED const char* const event_name,
                                                   const amxc_var_t* const data,
                                                   UNUSED void* const priv) {
    amxd_object_t* templ = amxd_dm_signal_get_object(dhcp_get_dm(), data);
    amxd_object_t* ipaddress = amxd_object_get_instance(templ, NULL, GET_UINT32(data, "index"));
    dhcps_client_option_ipaddress_update_dns_host(ipaddress);
}

void _dhcps_client_option_hostname_ipaddress_removed(UNUSED const char* const event_name,
                                                     const amxc_var_t* const data,
                                                     UNUSED void* const priv) {
    amxd_object_t* templ = amxd_dm_signal_get_object(dhcp_get_dm(), data);
    amxd_object_t* option_12 = amxd_object_findf(templ, ".^.Option.[Tag == 12].");
    when_null_trace(option_12, exit, ERROR, "No option 12 found to remove ip address");
    remove_host_ip(option_12, GETP_CHAR(data, "parameters.IPAddress"));
exit:
    return;
}

void _dhcps_client_option_hostname_ipaddress_changed(UNUSED const char* const event_name,
                                                     const amxc_var_t* const data,
                                                     UNUSED void* const priv) {
    amxd_object_t* ipaddress = amxd_dm_signal_get_object(dhcp_get_dm(), data);
    const char* ip_rm = GETP_CHAR(data, "parameters.IPAddress.from");
    if(!str_empty(ip_rm)) {
        amxd_object_t* option_12 = amxd_object_findf(ipaddress, ".^.^.Option.[Tag == 12].");
        when_null_trace(option_12, exit, ERROR, "No option 12 found to remove ip address");
        remove_host_ip(option_12, ip_rm);
    }
    dhcps_client_option_ipaddress_update_dns_host(ipaddress);
exit:
    return;
}

void _dhcps_client_option_hostname_active_changed(UNUSED const char* const event_name,
                                                  const amxc_var_t* const data,
                                                  UNUSED void* const priv) {
    amxd_object_t* client = amxd_dm_signal_get_object(dhcp_get_dm(), data);
    amxd_object_t* option_12 = amxd_object_findf(client, ".Option.[Tag == 12].");
    when_null(option_12, exit);
    set_host(option_12, DNS_HOST_ARG_CLIENT);
exit:
    return;
}
