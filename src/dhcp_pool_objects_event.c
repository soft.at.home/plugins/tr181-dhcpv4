/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "dhcp_priv.h"
#include "dhcp_event.h"
#include "dm_dhcp.h"
#include "dhcp_info.h"
#include "firewall_utility.h"

#define ME "event"

/**
 * @brief
 * Change the IsStatic parameter with the value in enable
 *
 * @param ip The ip of the lease to look for
 * @param mac The mac address of the client to look for
 * @param static_ip The static lease object
 * @param enable boolean value to set IsStatic to
 * @return true if a lease have been found and modified, false elsewhere
 */
static bool dhcp_client_set_is_static(char* ip, char* mac, amxd_object_t* static_ip, bool enable) {
    bool rv = false;
    amxd_object_t* client = NULL;
    amxd_object_t* lease = NULL;
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    when_str_empty_trace(ip, exit, ERROR, "Cannot get static address ip");
    when_str_empty_trace(mac, exit, ERROR, "Cannot get client mac address");

    TO_LOWER(mac);

    client = amxd_object_findf(static_ip, "^.^.Client.[Chaddr=='%s']", mac);
    when_null_trace(client, exit, INFO, "No client for %s", mac);
    lease = amxd_object_findf(client, "IPv4Address.[IPAddress=='%s']", ip);
    when_null_trace(lease, exit, INFO, "No lease for %s : %s", mac, ip);

    amxd_trans_select_object(&trans, lease);

    amxd_trans_set_value(bool, &trans, "IsStatic", enable);

    when_failed_trace(amxd_trans_apply(&trans, dhcp_get_dm()), exit, ERROR, "Cannot change IsStatic");
    rv = true;
exit:
    amxd_trans_clean(&trans);
    return rv;
}


/**
 * @brief
 * Adds a DHCPv4 server pool static IP address to the UCI files.
 * Called when a TR181 DHCPv4.Server.Pool.{i}.StaticAddress data model object instance is added.
 *
 * @param event_name event name
 * @param event_data event data
 * @param priv private data associated with event subscription
 */
void _dhcp_server_static_address_added(UNUSED const char* const event_name,
                                       const amxc_var_t* const event_data,
                                       UNUSED void* const priv) {
    int rc = 0;
    const char* name = NULL;
    char* ip_addr = NULL;
    char* mac_addr = NULL;
    bool enable = false;
    amxd_object_t* templ = NULL;
    amxd_object_t* addr = NULL;
    amxd_object_t* pool = NULL;
    amxd_dm_t* dm = NULL;

    when_null_trace(event_data, exit, ERROR, "Invalid event_data argument");
    SAH_TRACEZ_INFO(ME, "DHCPv4 server pool static IPv4 address has been added to TR-181 data model");

    name = GET_CHAR(event_data, "name");
    when_null_trace(name, exit, ERROR, "Cannot get object name.");

    dm = dhcp_get_dm();
    when_null_trace(dm, exit, ERROR, "Cannot get DHCPv4 data model.");
    templ = amxd_dm_signal_get_object(dm, event_data);
    when_null_trace(templ, exit, ERROR, "Cannot get template for pool object.");
    addr = amxd_object_get_instance(templ, name, 0);
    when_null_trace(addr, exit, ERROR, "Cannot get static address object.");
    pool = amxd_object_get_parent(amxd_object_get_parent(addr));
    when_null_trace(pool, exit, ERROR, "Cannot get pool object.");

    enable = amxd_object_get_value(bool, addr, "Enable", NULL);
    ip_addr = amxd_object_get_value(cstring_t, addr, "Yiaddr", NULL);
    mac_addr = amxd_object_get_value(cstring_t, addr, "Chaddr", NULL);
    if(enable) {
        if(str_empty(ip_addr)) {
            rc = dm_dhcp_static_address_assign(pool, addr);
            when_failed_trace(rc, exit, ERROR, "Problem updating DHCPv4 server pool static IPv4 address");
        } else {
            if(!str_empty(mac_addr)) {
                dhcp_client_set_is_static(ip_addr, mac_addr, addr, enable);
            }
            rc = dm_dhcp_static_address_enable(pool, addr);
            when_failed_trace(rc, exit, ERROR, "Problem adding DHCPv4 server pool static IPv4 address");
        }
    }
exit:
    free(ip_addr);
    free(mac_addr);
    return;
}

/**
 * @brief
 * Updates a DHCPv4 server pool static IP address in the UCI files.
 * Called when a TR181 DHCPv4.Server.Pool.{i}.StaticAddress data model object instance is changed.
 *
 * @param event_name event name
 * @param event_data event data
 * @param priv private data associated with event subscription
 */
void _dhcp_server_static_address_changed(UNUSED const char* const event_name,
                                         const amxc_var_t* const event_data,
                                         UNUSED void* const priv) {
    amxd_object_t* pool = NULL;
    amxd_object_t* addr = NULL;
    amxd_dm_t* dm = NULL;
    bool enable = false;
    char* ip_addr = NULL;
    int rc = 1;

    SAH_TRACEZ_INFO(ME, "DHCPv4 static ip has been changed in TR-181 data model");
    when_null_trace(event_data, exit, ERROR, "Invalid event_data argument");

    dm = dhcp_get_dm();
    when_null_trace(dm, exit, ERROR, "Cannot get DHCPv4 data model.");
    addr = amxd_dm_signal_get_object(dm, event_data);
    when_null_trace(addr, exit, ERROR, "Cannot get DHCPv4 static ip object.");
    pool = amxd_object_get_parent(amxd_object_get_parent(addr));
    when_null_trace(pool, exit, ERROR, "Cannot get pool of static ip");

    ip_addr = amxd_object_get_value(cstring_t, addr, "Yiaddr", NULL);
    enable = amxd_object_get_value(bool, addr, "Enable", NULL);

    if(enable) {
        if(str_empty(ip_addr)) {
            rc = dm_dhcp_static_address_assign(pool, addr);
            when_failed_trace(rc, exit, ERROR, "Problem updating DHCPv4 server pool static IPv4 address");
        } else {
            rc = dm_dhcp_static_address_enable(pool, addr);
            when_failed_trace(rc, exit, ERROR, "Problem adding DHCPv4 server pool static IPv4 address");
        }
    } else {
        rc = dm_dhcp_static_address_disable(NULL, pool, addr);
        when_failed_trace(rc, exit, ERROR, "Problem removing DHCPv4 server pool static IPv4 address");
    }
exit:
    free(ip_addr);
    return;
}

/**
 * @brief
 * Removes a DHCPv4 server pool static IP address from the UCI files.
 * Called when a TR181 DHCPv4.Server.Pool.{i}.StaticAddress data model object instance is removed.
 *
 * @param event_name event name
 * @param event_data event data
 * @param priv private data associated with event subscription
 */
void _dhcp_server_static_address_removed(UNUSED const char* const event_name,
                                         const amxc_var_t* const event_data,
                                         UNUSED void* const priv) {
    amxd_object_t* pool = NULL;
    amxd_object_t* addr = NULL;
    amxd_dm_t* dm = NULL;

    SAH_TRACEZ_INFO(ME, "DHCPv4 server pool has been removed from TR-181 data model");
    when_null_trace(event_data, exit, ERROR, "Invalid event_data argument");
    dm = dhcp_get_dm();
    when_null_trace(dm, exit, ERROR, "Cannot get DHCPv4 data model.");
    addr = amxd_dm_signal_get_object(dm, event_data);
    when_null_trace(addr, exit, ERROR, "Cannot get DHCPv4 static ip object.");
    pool = amxd_object_get_parent(addr);
    when_null_trace(pool, exit, ERROR, "Cannot find pool object");
    dm_dhcp_static_address_disable(event_data, pool, addr);
exit:
    return;
}

/**
 * @brief
 * Sets the TR181 DHCPv4.Server.Pool.{i}.Client.{i}.Active flag to false when the client is not using any IP addresses.
 * Called when a TR181 DHCPv4.Server.Pool.{i}.Client.{i}.IPv4AddressNumberOfEntries data model parameter is changed.
 *
 * @param event_name event name
 * @param event_data event data
 * @param priv private data associated with event subscription
 */
void _dhcp_server_client_active(UNUSED const char* const event_name,
                                const amxc_var_t* const event_data,
                                UNUSED void* const priv) {
    uint32_t from = 0;
    uint32_t to = 0;
    when_null_trace(event_data, exit, ERROR, "Invalid event_data argument");
    SAH_TRACEZ_INFO(ME, "DHCPv4 server pool client number of IP addresses has changed in TR-181 data model");

    from = GETP_UINT32(event_data, "parameters.IPv4AddressNumberOfEntries.from");
    to = GETP_UINT32(event_data, "parameters.IPv4AddressNumberOfEntries.to");
    if((from == 1) && (to == 0)) {
        const char* object = GETP_CHAR(event_data, "object");
        int rc = 0;
        when_null_trace(object, exit, ERROR, "No object in event_data");
        SAH_TRACEZ_INFO(ME, "Setting '%s' to false in TR-181 data model because DHCPv4 client is not leasing any IP addresses", object);
        rc = dm_dhcp_set_pool_client_active(object, false);
        when_failed_trace(rc, exit, ERROR, "Problem setting DHCPv4 server pool client '%s' inactive", object);
        dhcp_trigger_maintenance();
    }
exit:
    return;
}

/**
 * @brief Set the IsStatic parameter of a lease
 *
 * @param lease the lease object to set the IsStatic parameter
 */
static void set_is_static(amxd_object_t* lease) {
    amxd_trans_t trans;
    amxd_object_t* client = NULL;
    amxd_object_t* static_ips = NULL;
    amxd_object_t* static_ip = NULL;
    char* ip = NULL;
    char* mac = NULL;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    static_ips = amxd_object_findf(lease, "^.^.^.^.StaticAddress.");
    when_null_trace(static_ips, exit, ERROR, "Cannot get static ips object");
    client = amxd_object_findf(lease, "^.^.");
    when_null_trace(client, exit, ERROR, "Cannot get client object");

    ip = amxd_object_get_value(cstring_t, lease, "IPAddress", NULL);
    when_str_empty_trace(ip, exit, ERROR, "Cannot get lease ip");
    mac = amxd_object_get_value(cstring_t, client, "Chaddr", NULL);
    when_str_empty_trace(mac, exit, ERROR, "Cannot get client mac address");

    static_ip = amxd_object_findf(static_ips, "[Chaddr=='%s' && Yiaddr=='%s' && Enable==1]", mac, ip);
    if(static_ip == NULL) {
        TO_UPPER(mac);
        static_ip = amxd_object_findf(static_ips, "[Chaddr=='%s' && Yiaddr=='%s' && Enable==1]", mac, ip);
    }

    amxd_trans_select_object(&trans, lease);

    if(static_ip == NULL) {
        SAH_TRACEZ_INFO(ME, "No static ip for %s : %s", mac, ip);
        amxd_trans_set_value(bool, &trans, "IsStatic", false);
    } else {
        SAH_TRACEZ_INFO(ME, "Static ip found for %s : %s", mac, ip);
        amxd_trans_set_value(bool, &trans, "IsStatic", true);
    }

    when_failed_trace(amxd_trans_apply(&trans, dhcp_get_dm()), exit, ERROR, "Cannot change IsStatic");
exit:
    amxd_trans_clean(&trans);
    free(mac);
    free(ip);
    return;
}

/**
 * @brief
 * Whenever a lease is updated, check if it comes from a static address rule to set its IsStatic parameter
 *
 * @param event_name name of the event
 * @param event_data data of the event, the lease added
 * @param priv NULL
 */
void _dhcps_client_ipaddress_changed(UNUSED const char* const event_name,
                                     const amxc_var_t* const event_data,
                                     UNUSED void* const priv) {
    amxd_object_t* lease = NULL;

    when_null_trace(event_data, exit, ERROR, "Invalid event_data argument");

    lease = amxd_dm_signal_get_object(dhcp_get_dm(), event_data);
    when_null_trace(lease, exit, ERROR, "Cannot get IPv4Address object.");

    set_is_static(lease);
exit:
    return;
}

/**
 * @brief
 * Whenever a new lease is added, check if it comes from a static address rule to set its IsStatic parameter
 *
 * @param event_name name of the event
 * @param event_data data of the event, the lease added
 * @param priv NULL
 */
void _dhcp_client_check_if_static(UNUSED const char* const event_name,
                                  const amxc_var_t* const event_data,
                                  UNUSED void* const priv) {
    uint32_t index = 0;
    amxd_object_t* templ = NULL;
    amxd_object_t* lease = NULL;

    when_null_trace(event_data, exit, ERROR, "Invalid event_data argument");

    index = GET_UINT32(event_data, "index");
    when_false_trace(index != 0, exit, ERROR, "Cannot get object index.");

    templ = amxd_dm_signal_get_object(dhcp_get_dm(), event_data);
    when_null_trace(templ, exit, ERROR, "Cannot get template for IPv4Address object.");
    lease = amxd_object_get_instance(templ, NULL, index);
    set_is_static(lease);
exit:
    return;
}

/**
 * @brief
 * Check if the change of a static address rule needs to be linked to a present lease
 *
 * @param enable if the static address rule is enabled
 * @param event_data data of the event, what parameter of the static address have changed
 * @param static_ip the static address rule object
 */
static void check_if_changed_is_static_lease(bool enable,
                                             const amxc_var_t* const event_data,
                                             amxd_object_t* static_ip) {
    char* ip = NULL;
    char* mac = NULL;

    ip = amxc_var_dyncast(cstring_t, GETP_ARG(event_data, "parameters.Yiaddr.to"));
    if(ip == NULL) {
        ip = amxd_object_get_value(cstring_t, static_ip, "Yiaddr", NULL);
    }

    mac = amxc_var_dyncast(cstring_t, GETP_ARG(event_data, "parameters.Chaddr.to"));
    if(mac == NULL) {
        mac = amxd_object_get_value(cstring_t, static_ip, "Chaddr", NULL);
    }

    when_str_empty(ip, exit);
    when_str_empty(mac, exit);

    dhcp_client_set_is_static(ip, mac, static_ip, enable);
exit:
    free(mac),
    free(ip);
    return;
}

/**
 * @brief
 * Check if the static address rule was previously linked to a lease, change that lease IsStatic parameter if needed.
 *
 * @param enable if the rule is enabled
 * @param event_data data of the event, what parameter of the static address have changed
 * @param static_ip the static address rule object
 * @return true if a lease have been found and modified, false elsewhere
 */
static bool check_if_previous_is_static_lease(bool enable,
                                              const amxc_var_t* const event_data,
                                              amxd_object_t* static_ip) {
    bool changed = false;
    char* ip = NULL;
    char* mac = NULL;

    ip = amxc_var_dyncast(cstring_t, GETP_ARG(event_data, "parameters.Yiaddr.from"));
    if(ip == NULL) {
        ip = amxd_object_get_value(cstring_t, static_ip, "Yiaddr", NULL);
    } else {
        enable = false;
    }

    mac = amxc_var_dyncast(cstring_t, GETP_ARG(event_data, "parameters.Chaddr.from"));
    if(mac == NULL) {
        mac = amxd_object_get_value(cstring_t, static_ip, "Chaddr", NULL);
    } else {
        enable = false;
    }

    when_str_empty(ip, exit);
    when_str_empty(mac, exit);

    changed = dhcp_client_set_is_static(ip, mac, static_ip, enable);
exit:
    free(mac),
    free(ip);
    return changed;
}

/**
 * @brief
 * Set IsStatic to false or true if a lease was given by this static address rule
 *
 * @param event_name name of the event
 * @param event_data data of the event, parameters that have changed
 * @param priv NULL
 */
void _dhcp_change_static(UNUSED const char* const event_name,
                         const amxc_var_t* const event_data,
                         UNUSED void* const priv) {
    bool enable = false;
    bool changed = false;
    amxd_object_t* static_ip = NULL;
    amxc_var_t* enable_var = NULL;

    when_null_trace(event_data, exit, ERROR, "Invalid event_data argument");

    when_null_trace(dhcp_get_dm(), exit, ERROR, "Cannot get DHCPv4Server data model.");
    static_ip = amxd_dm_signal_get_object(dhcp_get_dm(), event_data);
    when_null_trace(static_ip, exit, ERROR, "Cannot get template for Static object.");

    enable_var = GETP_ARG(event_data, "parameters.Enable");
    if(enable_var == NULL) {
        enable = amxd_object_get_value(bool, static_ip, "Enable", NULL);
    } else {
        enable = GET_BOOL(enable_var, "to");
    }

    changed = check_if_previous_is_static_lease(enable, event_data, static_ip);
    when_true(changed, exit);
    check_if_changed_is_static_lease(enable, event_data, static_ip);
exit:
    return;
}

/**
 * @brief
 * Set IsStatic to false if a lease was given by this static address rule
 *
 * @param event_name name of the event
 * @param event_data data of the event, the removed static address
 * @param priv NULL
 */
void _dhcp_removed_static(UNUSED const char* const event_name,
                          const amxc_var_t* const event_data,
                          UNUSED void* const priv) {
    char* ip = NULL;
    char* mac = NULL;
    amxd_trans_t trans;
    amxd_object_t* static_ips = NULL;
    amxd_object_t* client = NULL;
    amxd_object_t* lease = NULL;
    amxd_dm_t* dm = NULL;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);


    when_null_trace(event_data, exit, ERROR, "Invalid event_data argument");

    dm = dhcp_get_dm();
    when_null_trace(dm, exit, ERROR, "Cannot get DHCPv4Server data model.");
    static_ips = amxd_dm_signal_get_object(dm, event_data);
    when_null_trace(static_ips, exit, ERROR, "Cannot get template for Static object.");

    ip = amxc_var_dyncast(cstring_t, GETP_ARG(event_data, "parameters.Yiaddr"));
    when_str_empty_trace(ip, exit, ERROR, "No addr given for the static address");

    mac = amxc_var_dyncast(cstring_t, GETP_ARG(event_data, "parameters.Chaddr"));
    when_str_empty_trace(mac, exit, ERROR, "No mac given for the static address");

    TO_LOWER(mac);

    client = amxd_object_findf(static_ips, "^.Client.[Chaddr=='%s']", mac);
    when_null_trace(client, exit, ERROR, "No client for %s", mac);
    lease = amxd_object_findf(client, "IPv4Address.[IPAddress=='%s']", ip);
    when_null_trace(lease, exit, ERROR, "No lease for %s : %s", mac, ip);

    amxd_trans_select_object(&trans, lease);

    amxd_trans_set_value(bool, &trans, "IsStatic", false);

    when_failed_trace(amxd_trans_apply(&trans, dm), exit, ERROR, "Cannot change IsStatic");
exit:
    amxd_trans_clean(&trans);
    free(mac),
    free(ip);
}

void _dhcp_option_add_or_remove(UNUSED const char* const event_name,
                                const amxc_var_t* const event_data,
                                UNUSED void* const priv) {
    amxd_object_t* pool = NULL;
    amxd_object_t* option_templ = NULL;
    amxd_dm_t* dm = NULL;
    dhcp_pool_info_t* info = NULL;
    bool enabled = false;

    dm = dhcp_get_dm();
    when_null_trace(dm, exit, ERROR, "Cannot get DHCPv4 data model.");
    option_templ = amxd_dm_signal_get_object(dm, event_data);
    when_null_trace(option_templ, exit, ERROR, "Cannot get DHCPv4 pool option object.");

    enabled = GETP_BOOL(event_data, "parameters.Enable");

    pool = amxd_object_get_parent(option_templ);
    when_null_trace(pool, exit, ERROR, "Can't get DHCPv4 pool object");

    info = (dhcp_pool_info_t*) pool->priv;
    when_null_trace(info, exit, ERROR, "DHCPv4 pool private data is NULL");

    if((info->flags.bit.enable == 1) && enabled) {
        when_failed_trace(dm_dhcp_update_pool_status(pool), exit, ERROR, "Failed to update DHCPv4 pool status");
    }
exit:
    return;
}

void _dhcp_option_changed(UNUSED const char* const event_name,
                          const amxc_var_t* const event_data,
                          UNUSED void* const priv) {
    amxd_object_t* pool = NULL;
    amxd_object_t* option = NULL;
    amxd_dm_t* dm = NULL;
    dhcp_pool_info_t* info = NULL;

    dm = dhcp_get_dm();
    when_null_trace(dm, exit, ERROR, "Cannot get DHCPv4 data model.");
    option = amxd_dm_signal_get_object(dm, event_data);
    when_null_trace(option, exit, ERROR, "Cannot get DHCPv4 pool option object.");

    pool = amxd_object_findf(option, "^.^.");
    when_null_trace(pool, exit, ERROR, "Can't get DHCPv4 pool object");

    info = (dhcp_pool_info_t*) pool->priv;
    when_null_trace(info, exit, ERROR, "DHCPv4 pool private data is NULL");

    if(info->flags.bit.enable == 1) {
        when_failed_trace(dm_dhcp_update_pool_status(pool), exit, ERROR, "Failed to update DHCPv4 pool status");
    }

exit:
    return;
}