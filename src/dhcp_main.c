/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_dm.h>
#include <amxm/amxm.h>


#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "netmodel/client.h"

#include "dhcp_priv.h"
#include "dm_dhcp.h"
#include "dhcp_event.h"
#include "firewall_utility.h"

#define ME "main"

#define DHCP_DEF_INTERVAL 120000

typedef struct _dhcp {
    amxd_dm_t* dhcp_dm;
    amxo_parser_t* parser;
    amxp_timer_t* maintenance_timer;
    bool is_order_in_trans;
} dhcp_t;

static dhcp_t dhcp;

/**
 * @brief
 * DHCPv4 server plugin cyclic maintenance callback function.
 * Removes inactive TR-181 DHCPv4.Server.Pool.{i}.Client instances.
 * Removes all inactive TR181 DHCPv4.Server.Pool.{i}.Client.{i}.IPv4Address instances.
 *
 * @param timer timer object
 * @param priv private data
 * @return 0 on success
 */
void dhcp_maintenance(UNUSED amxp_timer_t* timer, UNUSED void* priv) {
    dm_dhcp_delete_inactive_clients();
    dm_dhcp_delete_expired_leases();
    dhcp_trigger_maintenance();
}

/**
 * @brief
 * Starts the DHCP maintenance timer.
 */
void dhcp_trigger_maintenance(void) {
    uint32_t interval = DHCP_DEF_INTERVAL;
    amxp_timer_start(dhcp.maintenance_timer, interval);
}

/**
 * @brief
 * Callback triggered when the Time. object is available on the bus.
 */
static void dhcpv4_time_available_cb(UNUSED const char* signame,
                                     UNUSED const amxc_var_t* const data,
                                     UNUSED void* const priv) {
    int ret = 1;
    amxb_bus_ctx_t* time_ctx = amxb_be_who_has("Time.");
    when_null_trace(time_ctx, exit, ERROR, "No bus found for Time");
    ret = amxb_subscribe(time_ctx, "Time.", "notification == 'dm:object-changed' && path == 'Time.'", _dhcp_server_sys_time_updated, NULL);
    when_failed_trace(ret, exit, ERROR, "Failed to subscribe to Time events");
exit:
    return;
}

/**
 * @brief
 * Subscribe to other tr181 plugins:
 *  Time
 *
 * @return int 0 if succeeded
 */
static int dhcpv4_subscribe(void) {
    int ret = -1;

    amxb_bus_ctx_t* time_ctx = amxb_be_who_has("Time.");
    if(time_ctx == NULL) {
        amxb_wait_for_object("Time.");
        ret = amxp_slot_connect_filtered(NULL, "^wait:Time\\.$", NULL, dhcpv4_time_available_cb, NULL);
        when_failed_trace(ret, exit, ERROR, "Failed to wait for Time to be available");
        SAH_TRACEZ_WARNING(ME, "Waiting for Time");
        goto exit;
    } else {
        ret = amxb_subscribe(time_ctx, "Time.", "notification == 'dm:object-changed' && path == 'Time.'", _dhcp_server_sys_time_updated, NULL);
        when_failed_trace(ret, exit, ERROR, "Failed to subscribe to Time events");
    }
exit:
    return ret;
}

/**
 * @brief
 * Helper function to load the needed controller on the system.
 *
 * @param module_dir the directory where the module/controller can be found.
 * @param controller_name the name of the controller to be loaded.
 * @param so shared file object
 */
static int dhcpv4_load_controller(const char* module_dir, const char* controller_name, amxm_shared_object_t** so) {
    int retval = -1;
    amxd_object_t* dhcpv4_obj = amxd_dm_findf(dhcp_get_dm(), "%s", DHCP_SERVER);
    const amxc_var_t* controller =
        amxd_object_get_param_value(dhcpv4_obj, controller_name);
    const char* name = GET_CHAR(controller, NULL);
    amxc_string_t mod_path;

    amxc_string_init(&mod_path, 0);

    amxc_string_setf(&mod_path, "%s/%s.so", module_dir, name);
    SAH_TRACEZ_INFO(ME, "Loading controller '%s' (file = %s)",
                    name, amxc_string_get(&mod_path, 0));
    retval = amxm_so_open(so, name, amxc_string_get(&mod_path, 0));
    when_failed_trace(retval, exit, ERROR, "Loading controller '%s' failed with error %d", amxc_string_get(&mod_path, 0), retval);
exit:
    amxc_string_clean(&mod_path);
    return retval;
}

/**
 * @brief
 * Initialises the DHCPv4 server plugin.
 *
 * @param dm TR-181 data model
 * @param parser ODL parser
 * @return 0 on success
 */
static int dhcp_init(amxd_dm_t* dm, amxo_parser_t* parser) {
    int r = -1;
    amxm_shared_object_t* so = amxm_get_so("self");
    amxd_object_t* root_obj = amxd_dm_get_root(dm);
    amxd_object_t* dhcpv4server = amxd_dm_findf(dm, "%s", DHCP_SERVER);
    amxm_module_t* mod = NULL;
    amxc_var_t server_conf;
    const char* local_module_dir = NULL;

    SAH_TRACEZ_IN(ME);

    amxc_var_init(&server_conf);

    when_null_trace(so, exit, ERROR, "Unable to get shared object.");
    when_null_trace(root_obj, exit, ERROR, "Unable to get root object");
    when_null_trace(dhcpv4server, exit, ERROR, "Unable to get dhcpv4 server object");

    dhcp.dhcp_dm = dm;
    dhcp.parser = parser;
    dhcp.maintenance_timer = NULL;
    dhcp.is_order_in_trans = true;

    SAH_TRACEZ_INFO(ME, "Starting DHCPv4 server plugin...");

    when_failed(amxm_module_register(&mod, so, MOD_CORE), exit);
    amxm_module_add_function(mod, "set-lease", dm_dhcp_set_lease);
    amxm_module_add_function(mod, "remove-lease", dm_dhcp_remove_lease);

    local_module_dir = GET_CHAR(dhcp_get_config(), "module-server-dir");
    when_failed_trace(dhcpv4_load_controller(local_module_dir, "Controller", &so), exit, ERROR, "failed to load server controller");
    when_failed_trace(dhcpv4_load_controller("/usr/lib/amx/modules/", "FWController", &so), exit, ERROR, "failed to load FWController");
    when_false_trace(netmodel_initialize(), exit, ERROR,
                     "Failed to init netmodel");
    amxp_timer_new(&dhcp.maintenance_timer, dhcp_maintenance, NULL);
    amxp_timer_start(dhcp.maintenance_timer, DHCP_DEF_INTERVAL);

    //parse default
    r = amxo_parser_parse_string(parser, "?include '${odl.directory}/${name}.odl':'${odl.dm-defaults}';", amxd_dm_get_root(dm));
    if(r != 0) {
        SAH_TRACEZ_WARNING(ME, "Failed to parse defaults");
    }
    dhcp.is_order_in_trans = false;

    amxd_object_get_params(dhcpv4server, &server_conf, amxd_dm_access_protected);
    // add signal to the end of the queue to start back-end after the defaults
    amxd_object_emit_signal(root_obj, "dhcpv4-server-start-back-end", &server_conf);
    when_failed_trace(dhcpv4_subscribe(), exit, ERROR, "Failed to subscribe to other plugins");
    r = 0;
exit:
    amxc_var_clean(&server_conf);
    SAH_TRACEZ_OUT(ME);
    return r;
}

/**
 * @brief
 * Shuts down the DHCPv4 server plugin.
 *
 * @return 0 on success
 */
static int dhcp_cleanup(void) {
    int r = 0;
    SAH_TRACEZ_INFO(ME, "Stopping DHCPv4 server plugin...");
    firewall_cleanup();
    amxp_timer_delete(&dhcp.maintenance_timer);
    amxm_close_all();
    netmodel_cleanup();
    dhcp.dhcp_dm = NULL;
    dhcp.parser = NULL;
    dhcp.is_order_in_trans = false;
    return r;
}

/**
 * @brief
 * Gets the TR-181 data model object.
 *
 * @return TR-181 data model object.
 */
amxd_dm_t* dhcp_get_dm(void) {
    return dhcp.dhcp_dm;
}

/**
 * @brief
 * Gets the ODL parser object.
 *
 * @return ODL parser object.
 */
amxo_parser_t* dhcp_get_parser(void) {
    return dhcp.parser;
}

/**
 * @brief
 * Gets the ODL config object.
 *
 * @return ODL config object.
 */
amxc_var_t* dhcp_get_config(void) {
    return &(dhcp.parser->config);
}

/**
 * @brief
 * Return if the order are being modified
 *
 * @return boolean
 */
bool dhcp_order_in_trans(void) {
    return dhcp.is_order_in_trans;
}

/**
 * @brief
 * Set if the orders are being modified
 *
 * @param in_modification set if orders are being modified
 */
void dhcp_order_set_trans(bool in_modification) {
    dhcp.is_order_in_trans = in_modification;
}

/**
 * @brief
 * ODL entry point function for the DHCPv4 server plugin.
 *
 * @return 0 on success
 */
int _dhcp_main(int reason, amxd_dm_t* dm, amxo_parser_t* parser) {
    int r = 0;
    switch(reason) {
    case 0:
        r = dhcp_init(dm, parser);
        break;
    case 1:
        r = dhcp_cleanup();
        break;
    }
    return r;
}
